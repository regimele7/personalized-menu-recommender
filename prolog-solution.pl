% Knowledge Base

% Facts for ingredients: ingredient(name, [dietary_restrictions], [allergies])
ingredient(flour, [], [gluten]).
ingredient(milk, [vegan], [dairy]).
ingredient(eggs, [vegan], []).
ingredient(butter, [vegan], [dairy]).
ingredient(baking_powder, [], []).
ingredient(yogurt, [vegan], [dairy]).
ingredient(bread, [], [gluten]).
ingredient(meat, [vegan, vegetarian], []).
ingredient(chicken_patty, [vegan, vegetarian], []).
ingredient(chicken, [vegan, vegetarian], []).
ingredient(beef, [vegan, vegetarian], []).
ingredient(salmon, [vegan, vegetarian], []).
ingredient(cheese, [vegan], [dairy]).
ingredient(tomatoes, [vegan], []).
ingredient(rice, [], []).
ingredient(beans, [], []).
ingredient(tofu, [], []).
ingredient(lettuce, [], []).
ingredient(spinach, [], []).
ingredient(bell_pepper, [], []).
ingredient(onion, [], []).
ingredient(avocado, [], []).
ingredient(quinoa, [], []).

% Facts for meals meal(name, [ingredients], nutrition_goal)
meal(pancakes, [flour, eggs, milk, baking_powder], no_specific_goals).
meal(omelette, [eggs, cheese, vegetables], muscle_gain).
meal(yogurt_parfait, [yogurt, granola, fruits], weight_loss).
meal(vegan_sandwich, [whole_grain_bread, hummus, vegetables], no_specific_goals).
meal(chicken_burger, [chicken_patty, lettuce, tomato, onion], no_specific_goals).
meal(spicy_burger, [beef, whole_wheat_bun, lettuce, tomato, onion, spicy_sauce], muscle_gain).
meal(steak, [beef], muscle_gain).
meal(vegetarian_pizza, [pizza_dough, tomato_sauce, mozzarella_cheese, vegetables], no_specific_goals).
meal(roast_chicken, [chicken], weight_loss).
meal(grilled_salmon, [salmon], weight_loss).
meal(vegan_buddha_bowl, [quinoa, tofu, vegetables, avocado], weight_loss).

% Facts for time of day -> time_of_day(time, [meals])
time_of_day(breakfast, [pancakes, omelette, yogurt_parfait, vegan_sandwich]).
time_of_day(lunch, [chicken_burger, spicy_burger, vegan_sandwich, vegetarian_pizza, vegan_buddha_bowl]).
time_of_day(dinner, [steak, vegetarian_pizza, roast_chicken, grilled_salmon, vegan_buddha_bowl]).

nutrition_goal_profile(NutritionGoal, Meals) :-
    findall(
        Meal,
        (meal(Meal, _, NutritionGoal)),
        Meals).


dietary_restrictions_profile(Restriction, Meals) :-
    findall(Meal, (meal(Meal, Ingredients, _),
                   \+ meal_contains_restriction(Ingredients, Restriction)),
            Meals).

meal_contains_restriction([], _Restriction) :- fail.
meal_contains_restriction([Ingredient|_], Restriction) :-
    ingredient(Ingredient, Restrictions, _),
    member(Restriction, Restrictions).
meal_contains_restriction([_|RestIngredients], Restriction) :-
    meal_contains_restriction(RestIngredients, Restriction).
    

allergies_profile(Allergy, Meals) :-
    findall(Meal, (meal(Meal, Ingredients, _),
                   \+ meal_contains_allergy(Ingredients, Allergy)),
            Meals).

meal_contains_allergy([], _Allergy) :- fail.
meal_contains_allergy([Ingredient|_], Allergy) :-
    ingredient(Ingredient, _, Allergies),
    member(Allergy, Allergies).
meal_contains_allergy([_|RestIngredients], Allergy) :-
    meal_contains_allergy(RestIngredients, Allergy).


meal_matches_criteria(TimeOfDay, Restrictions, Allergies, NutritionGoal, Meal) :-
    time_of_day(TimeOfDay, MealsForTime),
    member(Meal, MealsForTime),
    meal(Meal, Ingredients, Goal),
    ( Goal = NutritionGoal ; NutritionGoal = no_specific_goals ),
    \+ (member(Restriction, Restrictions), meal_contains_restriction(Ingredients, Restriction)),
    \+ (member(Allergy, Allergies), meal_contains_allergy(Ingredients, Allergy)).

suggest_meal(TimeOfDay, Restrictions, Allergies, NutritionGoal, TimeAndMeals) :-
    time_of_day(TimeOfDay, AllPossibleMeals),
    setof(Meal, Ingredients^Goal^(
        member(Meal, AllPossibleMeals),
        meal(Meal, Ingredients, Goal),
        ( Goal = NutritionGoal ; NutritionGoal = no_specific_goals ),
        \+ (member(Restriction, Restrictions), meal_contains_restriction(Ingredients, Restriction)),
        \+ (member(Allergy, Allergies), meal_contains_allergy(Ingredients, Allergy))
    ), Meals),
    TimeAndMeals = (TimeOfDay, Meals).



