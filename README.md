# Meal Recommendation System

This Prolog-based Meal Recommendation System suggests meals based on various criteria such as time of day, dietary restrictions, allergies, and nutrition goals.

## Knowledge Base

- Ingredients
- Meals
- Time of Day

## Key Predicates

- `nutrition_goal_profile`
- `dietary_restrictions_profile`
- `allergies_profile`
- `suggest_meal`

## Example Usage

```prolog
suggest_meal(breakfast, [vegan], [dairy], weight_loss, TimeAndMeals).
